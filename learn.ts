// Run on the commandline using "npx ts-node ./learn.ts"

// Primitive
const bool: boolean = false;
const str: string = 'Hello World'
const num: number = 123
const notDefined: undefined = undefined;
const nullVariable: null = null
const bigger: BigInt = 9007199254740991n
// ------
let regex: RegExp = new RegExp('ab+c')
let array: Array<number> = [1, 2, 3,]
let set: Set<number> = new Set([1, 3, 4])

// Generics
class Queue<T> {
  data: T[] = [];
  push(item: T): void { this.data.push(item); }
  pop(): T | undefined { return this.data.shift() }

}

let numberQueue: Queue<number> = new Queue<number>()
numberQueue.push(2)
// numberQueue.push('2') // string will throw an error

let strQueue: Queue<string> = new Queue<string>();
strQueue.push('has to be a string');


let tuple: [number, number] = [1, 2]

// Special Types
let exAny: any;
let exUnknown: unknown;

// While both can be assigned to any kind of data type

exAny = 1;
exAny = 'latest'

exUnknown = 1;
exUnknown = 'latest unknown'

// the way they are accessed is different, while any allows you to go around, unknown ensures you validate a type before you can perform operations specific to that data type
let latestNum: number;

// latestNum = exUnknown + 2; 
latestNum = exAny + 2;

if (typeof exUnknown === 'number') {
  latestNum = exUnknown + 2
}

// Type assertion
const load = (): unknown => `my names`
let hello = load();
const trimmedText = (hello as string).trim()
const trimmedText2 = (<string>hello).trim() // this syntax does not work in tsx files
let idealTextApproach;
if (typeof hello === 'string') {
  idealTextApproach = hello.trim();
}

// Type casting
const strAge = '28'
// const numAgeWontWork: number = strAge as number
const numAge: number = Number(strAge)
console.log({ numAge })

// Type declaration - used for variables that need to be declared with its type without being assigned.
// the type declaration can be written as a separate file in fileName.d.ts
// its picked up so far the name ends with .d.ts
// console.log(process.env)
// declare const process: any

// readonly modifier
// disallows objects to be reassigned directly with the key or properties

type FinalPoint = {
  readonly x: number
  readonly y: number
}
type ChangeablePoint = {
  x: number
  y: number
}

type ReadOnlyChangeablePoint = Readonly<ChangeablePoint>
type MakeItPartialChangeablePoint = Partial<ChangeablePoint>
type RequiredChangeablePoint = Required<MakeItPartialChangeablePoint>

const fPoint: FinalPoint = { x: 30, y: 20 }
const cPoint: ChangeablePoint = { x: 30, y: 20 }

cPoint.x = 0 // possible
// fPoint.x = 0 // not allowed because it is read only. It cannot be reassigned

// Union Type
const nickName: string | number = 2;

type Square = {
  size: number
}
type Rectangle = {
  width: number
  height: number
}

type Shape = Square | Rectangle;

function area(shape: Shape) {
  if ('size' in shape) {
    return shape.size * shape.size
  }
  if ('width' in shape) {
    return shape.width * shape.height
  }
}
function areaUpdated(shape: Shape) {
  // User defined Type guard - shape is Square
  const isSquare = (shape: Shape): shape is Square => {
    return 'size' in shape
  }
  const isRectangle = (shape: Shape): shape is Rectangle => {
    return 'width' in shape
  }
  if (isSquare(shape)) {
    return shape.size * shape.size
  }
  if (isRectangle(shape)) {
    return shape.width * shape.height
  }
}

console.log(area({ size: 4 }))
console.log(area({ height: 10, width: 12 }))
console.log(area({ size: 7, width: 15 })) // how do we prevent this without having to specify a common denominator?


class LongSyntaxPerson {
  public name: string;
  public age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }
}

class ShortSyntaxPerson {
  constructor(
    public name: string,
    public age: number
  ) { }
}

const shortMan = new ShortSyntaxPerson('Williams', 20);
const longMan = new ShortSyntaxPerson('Henry', 15);
console.log({ short: shortMan })
console.log({ long: longMan })


// Interesting: double equals to null is same as undefined
console.log({ nullResponse: null == null })
console.log({ unDefinedResponse: null == undefined })

// Intersection Type
type Person = {
  name: string
}

type Email = {
  [email: string]: string
}

type Phone = {
  phone: string
}

type ContactDetails = Person & Email & Phone;


const contactUser = (user: ContactDetails): string => {
  return `Dear ${user.name},
  I hope you're good. The parcel has been sent to you at ${user.email} and we will call you shortly to get your review at ${user.phone}
  `
}

console.log(contactUser({ name: 'Suulola', email: 'oluwaseyi@suulola.com', phone: '0706' }))


// Non-null assertion operator
const apiResult: unknown = { data: { answer: 'this is the answer' } }

let user: ContactDetails;

// Or use a Definite assignment assertion to tell typescript the variable is assigned even if not assigned directly
// let user !: ContactDetails

function userUpdate() {
  user = { name: 'Suulola', email: 'olu@gmail.com', phone: '081' }
}
// console.log(`Updated value: `, user.name) // Typescript doesnt know user has been updated using the userUpdate function and therefore has been assigned and updated.

console.log(`Updated value: `, user!.name) // we need to tell typescript we know user.name isn't null by using the non-null assertion


// never key word
// The never type is used when you are sure that something is never going to occur.
function throwError(errorMsg: string): never {
  throw new Error(errorMsg);
}

// Call signature

type LongClassType = new (x: number, y: number) => { x: number, y: number };
type ShortClassType = {
  new(x: number, y: number): { x: number, y: number };
}
type LongFunction = (x: number, y: number) => number
type ShortFunction = {
  (x: number, y: number): number
}

const FirstClass: LongClassType = class {
  constructor(public x: number, public y: number) { }
}
const SecondClass: LongClassType = class {
  constructor(public x: number, public y: number) { }
}

// Abstract class 
// ReadonlyArray

// const assertion
const layout = (setting: {
  align: 'left' | 'right' | 'center',
  padding: number
}) => {
  console.log({ setting })
}

const params = { align: 'left', padding: 0 }
const updatedParams = { align: 'left' as const, padding: 0 } // const assertion makes it readonly and a literal left rather than just a string

// layout(params) // an error is thrown
layout(updatedParams) // an error is thrown

// this 
// this value in a function is determined by 
// 1. It's CALLING CONTEXT for regular functions - meaning, this is determined by the object that calls it or it is initialized to
// 2. It is LEXICALLY SCOPED for arrow functions - meaning this is determined by its parent or where it originated from and therefore more stable.

// typeof type operator
const student = { name: { first_name: `Oluwaseyi`, last_name: `Suulola` }, class: 'SS 3', subjects: ['Maths'] }
type StudentType = typeof student;
const anotherStudent: StudentType = {}
const lastStudent: typeof student = {}
// Lookup Types
type StudentNameType = StudentType['name']
const student_name: StudentNameType = { first_name: 'Good', last_name: 'dfsd' }

// keyof type operator
type StudentFields = keyof StudentType; //"name" | "class" | "subjects"
// keyof type operator with generics
function printObjectInfo<Obj, Key extends keyof Obj>(obj: Obj, key: Key) {
  const value = obj[key];
  console.log(`Can you see ${value} from Here`)
  return value
}

printObjectInfo(student, 'cool')

// Conditional types
type IsNumber<T> = T extends number ? number : 'others'
type withOthers = IsNumber<string>;

const age: IsNumber<number> = 12


// Infer keyword 
const createPerson = (firstName: string, lastName: string) => {
  return {
    firstName,
    lastName,
    fullName: `${firstName} ${lastName}`
  }
}

// infer the type from the function above
type FnReturnType<T> = T extends (...args: any) => infer R ? R : never;
// using custom method
type PersonType = FnReturnType<typeof createPerson>
// using typescript built in implementation instead
type PersonType2 = ReturnType<typeof createPerson>

const printPerson = (data: ReturnType<typeof createPerson>): string => {
  return `The name is ${data.firstName} and ${data.lastName} which equals ${data.fullName}`
}

// Template LiteralType
type CSSValue = number | `${number}px` | `${number}em`
let width: CSSValue = '20px'

type Size = 'small' | 'medium' | 'large'
type Color = 'primary' | 'secondary'
type Style = `${Size}-${Color}`

const applyStyle = (style: Style) => { }
applyStyle('large-primary')
applyStyle('large-good')

type LandOwnerType = Record<string, { name: string, numberOfPlots: number }>
type LandOwnerTypeVerbose = { [key: string]: { name: string, numberOfPlots: number } }
type PageType = Record<'home' | 'service', string[]>


type PaddingType = 'small' | 'normal' | 'large' | string // specifying the string directly makes us lose the typescript auto-complete
const paddingValue: PaddingType = '8px'
// best way to preserve this is

type Padding2Type = 'small' | 'normal' | 'large' | (string & {})
const padding2Value: Padding2Type = 'large'
const padding2Value_2: Padding2Type = '10px'


// Project Reference - Referencing projects that are outside the root folder
// 1. Add `composite: true` to the tsconfig of the project you want to reference
// 2. Add `references: [{path: "path to that project above"}]` to the project that does the consumption
// 3. When building the project that does the consumption. Instead of `build: tsc -p .`, use `build: tsc --build .` to include the referenced project when building your app

// Additional Resource: https://basarat.gitbook.io/typescript/getting-started
